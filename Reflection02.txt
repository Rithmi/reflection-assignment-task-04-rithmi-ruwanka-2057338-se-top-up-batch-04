package reflection;

//This experiment demonstrates the useof access modifiers in Java
//public methods & variables are visible to other class
//private methods & variables are not visible to other class

public class Reflection02 {

	public static void main(String[] args) {
		Simple s = new Simple(null, null, 0);
		int b = s.b; 
		System.out.println("s=" + s);		
	}
}
