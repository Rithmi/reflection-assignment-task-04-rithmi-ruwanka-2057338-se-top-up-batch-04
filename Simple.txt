package reflection;

public class Simple {
	  
	private final String title;
	private final String author;
	private final int numberofPages;
	public int b;

	public Simple (String title,  String author, int numberofPages) {
		this.title = title;
		this.author = author;
		this.numberofPages = numberofPages;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public int getNumberOfPages() {
		return numberofPages;
	}
	
	public String toString() {
		return String.format("\"%s\" by \"%s\" has %d pages", title, author, numberofPages);
	}
}
